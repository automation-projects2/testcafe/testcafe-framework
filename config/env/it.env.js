module.exports = {
  baseUrl: '',
  environment: '',
  apiEndPoint: '',
  routingKey: '',
  aemURL:
    '',
  sso_url: '',
  hash_id: '',
  hashURLPartOne:
    '',
  hashURLPartTwo:
    '',
  transactionSiteURL: ''
}
