const base = require('../config/env')
const devEnv = require('../config/env/dev.env')
const devUsers = require('../config/users/users.dev')
const itEnv = require('../config/env/it.env')
const itUsers = require('../config/users/users.it')
const uatEnv = require('../config/env/uat.env')
const uatUsers = require('../config/users/users.uat')

module.exports = {
  base,
  dev: {
    ...devEnv,
    ...devUsers
  },
  it: {
    ...itEnv,
    ...itUsers
  },
  uat: {
    ...uatEnv,
    ...uatUsers
  }
}
