const envObj = require('./envMap')

const getEnvConfig = () => {
  const envVariable =
    process.env.NODE_ENV === undefined ? 'dev' : process.env.NODE_ENV
  return {
    ...envObj.base,
    ...envObj[envVariable]
  }
}

export default getEnvConfig
