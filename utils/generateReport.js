const report = require('multiple-cucumber-html-reporter')
//const path = require('stepDefs\models\loginModel.js')
// const projectName = path.basename(__dirname);
const projectName = 'Invoice Summary Fixture'
const projectVersion = process.env.npm_package_version
const reportGenerationTime = new Date()
report.generate({
  reportName: 'TestCafé Report',
  jsonDir: 'TestCafeReports',
  reportPath: 'TestCafeReports/html',
  openReportInBrowser: true,
  disableLog: true,
  displayDuration: true,
  displayReportTime: true,
  durationInMS: true,
  customData: {
    title: 'Run info',
    data: [
      { label: 'Project', value: `${projectName}` },
      { label: 'Release', value: `${projectVersion}` },
      { label: 'Report Generation Time', value: `${reportGenerationTime}` }
    ]
  }
})
