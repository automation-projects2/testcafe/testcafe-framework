import { ClientFunction, t } from 'testcafe'
import fetch from 'isomorphic-unfetch'
import https from 'https'
import getEnvConfig from './getEnvConfig'

export const sessionStorageSet = ClientFunction((key, val) => {
  sessionStorage.setItem(key, val) // eslint-disable-line
})

export const sessionStorageGet = ClientFunction(
  (key) => sessionStorage.getItem(key) // eslint-disable-line
)
const config = getEnvConfig()

const getUserTokenName = ClientFunction(
  () => window.btoa('Nax079oqkEe7OopxB24kB8mNSAt5oYf1:QJaO6MHTzmwKczdK') // eslint-disable-line
)

const getUserTokenNameUAT = ClientFunction(
  () => window.btoa('4dTD384DfjsG61NAPhk9tOGSg2rtehrI:G2wpNV19ducldG3J') // eslint-disable-line
)
const selectEnv = () => {
  return config.environment === 'UAT'
    ? getUserTokenNameUAT()
    : getUserTokenName()
}

export const getLocation = ClientFunction(() => window.location.href) // eslint-disable-line

export const getHost = ClientFunction(() => window.location.host) // eslint-disable-line

export async function getToken(userID) {
  const clientd = {
    ChannelID: 'WEB',
    CompanyId: '654',
    Identifier: userID,
    IdentifierType: 'SignOnID',
    SessionKey: `cczxsdf${new Date()}ffff`,
    OMA99: true,
    ANT99: true,
    appid: 'OLB',
    'EASE.expiryTimeInMsecs': '1500',
    DeviceData: {
      IsTouchIDRegistered: false,
      IsFaceIDRegistered: true,
      DeviceParams: {}
    }
  }
  const agent = new https.Agent({
    rejectUnauthorized: false
  })

  const urlencoded = new URLSearchParams()
  urlencoded.append('customData', JSON.stringify(clientd))
  urlencoded.append('grant_type', 'client_credentials')
  urlencoded.append('Service-Version', '2')
  urlencoded.append('routingKey', 'it2')

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Basic ${await selectEnv()}`
    },
    body: urlencoded,
    redirect: 'follow',
    agent
  }
  const response = await fetch(
    config.environment === 'UAT'
      ? ''
      : '',
    requestOptions
  )
  const data = await response.json()
  return data.accessToken
}

export const setUserInfo = async (userId) => {
  const accessToken = await getToken(userId)
  await sessionStorageSet('AccessToken', accessToken)
  await t.wait(500)
  await t.expect(sessionStorageGet('AccessToken')).eql(accessToken)
}
