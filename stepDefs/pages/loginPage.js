const { Selector } = require('testcafe')

class DirectLoginPageElement {
  username = function () {
    return Selector('input').withAttribute('name', 'Username')
  }

  password = function () {
    return Selector('input').withAttribute('name', 'Password')
  }

  loginBtn = function () {
    return Selector('#login-button-continue')
  }
}

export default new DirectLoginPageElement()
