const { Given } = require('cucumber')
import LoginModel from './models/loginModel'

Given('User has direct logged in', async function () {
  await LoginModel.LaunchDirectLogin('primaryUser')
})

Given('User has SSO logged in', async function () {
  await LoginModel.LoginSSO()
})
