import { t } from 'testcafe'
import getEnvConfig from '../../utils/getEnvConfig'
import DirectLoginPageElement from '../pages/loginpage'

class LoginModel {
  constructor() {
    //login page
    this.usernameTB = DirectLoginPageElement.username()
    this.passwordTB = DirectLoginPageElement.password()
    this.loginBtn = DirectLoginPageElement.loginBtn()
  }

  LoginSSO = async () => {
    console.log('Entering LoginSSO URL')
    await t.maximizeWindow()
    const loginURL =
      getEnvConfig().hashURLPartOne +
      getEnvConfig().hash_id +
      getEnvConfig().hashURLPartTwo
    await t.navigateTo(loginURL)
  }

  LaunchDirectLogin = async (userType) => {
    console.log('Direct Login')
    await t
      .maximizeWindow()
      .navigateTo(getEnvConfig().transactionSiteURL)
      .typeText(this.usernameTB, getEnvConfig()[userType].userId)
      .typeText(this.passwordTB, getEnvConfig()[userType].password)
      .click(this.loginBtn)
    console.log('Successfully launched')
  }
}
export default new LoginModel()
