import { Selector, t, ClientFunction } from 'testcafe'
import { axeCheck, createReport } from "axe-testcafe"

fixture('Accessibility testing').page('')
test("Accessibility testing", async t => {
    await t.maximizeWindow()
    const { error, violations } = await axeCheck(t)
    // console.log(violations)
    await t.expect(violations.length === 0).ok(createReport(violations))
  })